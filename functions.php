<?php
/**
 * The main function definitions for the MC3 theme
 * @version 1.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( 'You do not have permission to access this file directly.' );
}

if ( ! class_exists( 'MC3' ) ) {
	class MC3 {
		/**
		 * Holds the version number for use with various assets
		 *
		 * @since  1.0.1
		 * @access public
		 * @var    string
		 */
		public $version = '1.0.1';
		
		/**
		 * Holds the class instance.
		 *
		 * @since   1.0.1
		 * @access	private
		 * @var		MC3
		 */
		private static $instance;
		
		/**
		 * Returns the instance of this class.
		 *
		 * @access  public
		 * @since   1.0.1
		 * @return	MC3
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) ) {
				$className = __CLASS__;
				self::$instance = new $className;
			}
			return self::$instance;
		}
		
		/**
		 * Create the object and set up the appropriate actions
		 */
		private function __construct() {
			//* Child theme (do not remove)
			define( 'CHILD_THEME_NAME', 'MC3' );
			define( 'CHILD_THEME_URL', 'https://mc3.depressioncenter.org/' );
			define( 'CHILD_THEME_VERSION', $this->version );
			
			add_action( 'wp_enqueue_scripts', array( $this, 'add_scripts_and_styles' ) );
			add_action( 'template_redirect', array( $this, 'template_redirect' ) );
			
			add_action( 'after_setup_theme', array( $this, 'adjust_genesis' ) );
			add_action( 'genesis_setup', array( $this, 'register_sidebars' ) );
			
			add_filter( 'body_class', array( $this, 'add_body_class' ) );
			
			add_image_size( 'home-feature', 1140, 410, true );
			
			//* Add support for custom header
			add_theme_support( 'custom-header', array(
				'flex-height'     => true,
				'flex-width'      => true, 
				'header-selector' => '.site-title a',
				'header-text'     => false,
			) );
			add_theme_support( 'genesis-footer-widgets', 1 );
			
			add_filter( 'wpv_filter_force_template', array( $this, 'is_content_template' ), 99, 3 );
		}
		
		/**
		 * Check to see if a Content Template is applied to this content
		 */
		function is_content_template( $selected, $id, $kind ) {
			if ( 100 !== intval( $selected ) )
				return $selected;
			
			add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
			return $selected;
		}
		
		/**
		 * Add some custom CSS classes to the body element
		 */
		function add_body_class( $classes=array() ) {
			$classes[] = 'nojs';
			
			return $classes;
		}
		
		/**
		 * Set up any necessary scripts and style sheets
		 */
		function add_scripts_and_styles() {
			wp_enqueue_script( 'mc3-modernizr', get_stylesheet_directory_uri() . '/scripts/modernizr-custom.js', array(), $this->version, true );
			wp_enqueue_script( 'mc3-2016', get_stylesheet_directory_uri() . '/scripts/mc3.js', array( 'jquery', 'mc3-modernizr' ), $this->version, true );
			wp_register_style( 'genesis', get_stylesheet_uri(), array(), $this->version, 'all' );
			wp_enqueue_style( 'mc3-2016', get_stylesheet_directory_uri() . '/mc3.css', array( 'genesis', 'dashicons' ), $this->version, 'all' );
		}
		
		/**
		 * Perform any actions that need to happen before the page is rendered
		 */
		function template_redirect() {
			if ( is_front_page() ) {
				add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
				remove_all_actions( 'genesis_loop' );
				add_action( 'genesis_loop', array( $this, 'do_front_page_widgets' ) );
			} else if ( is_singular() && is_main_query() ) {
				remove_all_actions( 'genesis_entry_header' );
				add_action( 'genesis_entry_header', array( $this, 'entry_header' ) );
				
				$content_template = get_post_meta( get_the_ID(), '_views_template', true );
				if ( 100 === intval( $content_template ) ) {
					add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
				}
				
				if ( is_page( 'pcp' ) ) {
					add_filter( 'body_class', function( $classes=array() ) { $classes[] = 'pcp'; return $classes; } );
				}
			}
		}
		
		/**
		 * Perform any actions that need to change the way Genesis behaves
		 */
		function adjust_genesis() {
			do_action( 'mc3_pre_adjust_genesis' );
			
			remove_action( 'genesis_meta', 'genesis_load_stylesheet' );
			add_theme_support( 'html5', array( 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ) );
			add_theme_support( 'genesis-responsive-viewport' );
			add_theme_support( 'genesis-accessibility', array( '404-page', 'drop-down-menu', 'headings', 'rems', 'search-form', 'skip-links' ) );
			add_theme_support( 'genesis-structural-wraps', array(
				'header',
				'nav',
				'subnav',
				'site-inner',
				'footer-widgets',
				'footer'
			) );
			add_theme_support( 'genesis-menus' , array( 'primary' => __( 'Primary Navigation Menu', 'capon-2016' ) ) );
			remove_action( 'genesis_after_header', 'genesis_do_nav' );
			remove_action( 'genesis_after_header', 'genesis_do_subnav' );
			add_action( 'genesis_before_header', 'genesis_do_nav', 9 );
			
			remove_all_actions( 'genesis_sidebar' );
			remove_all_actions( 'genesis_sidebar_alt' );
			add_action( 'genesis_sidebar', array( $this, 'do_sidebar' ) );
			
			/*remove_action( 'genesis_header', 'genesis_do_header' );
			add_action( 'genesis_header', array( $this, 'do_header' ) );*/
			remove_action( 'genesis_footer', 'genesis_do_footer' );
			remove_action( 'genesis_before_footer', 'genesis_footer_widget_areas' );
			add_action( 'genesis_footer', array( $this, 'do_footer' ) );
			
			do_action( 'mc3_post_adjust_genesis' );
			
			foreach ( array( 'content-sidebar', 'content-sidebar-sidebar', 'sidebar-sidebar-content', 'sidebar-content-sidebar' ) as $l ) {
				genesis_unregister_layout( $l );
			}
		}
		
		/**
		 * Output a modified version of the main page/entry header
		 */
		function entry_header() {
?>
<header class="entry-header">
<?php
			$page_title = get_the_title();
			global $post;
			if ( empty( $post->post_parent ) ) {
?>
	<h1 class="entry-title" itemprop="headline">
    	<?php echo $page_title ?>
    </h1>
<?php
			} else {
				$parents = get_ancestors( get_the_ID(), get_post_type( $post ) );
				$top = array_pop( $parents );
				$section_title = get_the_title( $top );
?>
	<h2 class="section-title">
    	<?php echo $section_title ?>
    </h2>
    <h1 class="page-title" itemprop="headline">
    	<?php echo $page_title ?>
    </h1>
<?php
			}
?>	
</header>
<?php
		}
		
		/**
		 * Register any new widget areas that are necessary
		 */
		function register_sidebars() {
			genesis_register_sidebar( array(
				'id'          => 'front-main', 
				'name'        => __( '[Front Page] Main Content' ), 
				'description' => __( 'Acts as the main content area for the front page of the website' ), 
			) );
			genesis_register_sidebar( array(
				'id'          => 'front-features', 
				'name'        => __( '[Front Page] Feature Items' ), 
				'description' => __( 'Appears below the main content area on the front page. Items are split equally horizontally on larger displays.' ), 
			) );
			genesis_register_sidebar( array( 
				'id'          => 'front-secondary', 
				'name'        => __( '[Front Page] Secondary Content' ), 
				'description' => __( 'Appears below the front page feature items; acts as secondary content for the front page.' ), 
			) );
		}
		
		/**
		 * Output the custom header area for this theme
		 */
		function do_header() {
		}
		
		/**
		 * Output the custom footer area for this theme
		 */
		function do_footer() {
			if ( ! is_active_sidebar( 'footer-1' ) )
				return;
			
			dynamic_sidebar( 'footer-1' );
		}
		
		/**
		 * Output the custom primary sidebar for this theme
		 */
		function do_sidebar() {
			if ( is_active_sidebar( 'sidebar' ) ) {
				dynamic_sidebar( 'sidebar' );
			} else {
				$this->do_related_pages();
			}
		}
		
		/**
		 * Output a menu of related pages
		 */
		function do_related_pages() {
			if ( ! is_page() ) {
				return;
			}
			
			$page = get_queried_object();
			$args = array(
				'sort_column' => 'menu_order', 
				'title_li'    => '', 
				'echo'        => 0, 
				'post_type'   => $page->post_type, 
			);
			if ( property_exists( $page, 'post_parent' ) && ! empty( $page->post_parent ) ) {
				$parents = get_ancestors( $page->ID, $page->post_type );
				if ( is_array( $parents ) )
					$parent = array_pop( $parents );
				else
					$parent = $page->post_parent;
					
				$args['child_of'] = $parent;
			} else {
				$args['child_of'] = $page->ID;
			}
			
			$tmp = get_post( $args['child_of'] );
			if ( 'pcp' == $tmp->post_name ) {
				return $this->do_pcp_related_pages( $parent );
			}
			
			$children = wp_list_pages( $args );
			if ( empty( $children ) || is_wp_error( $children ) )
				return;
			
			printf( '
		<div class="related widget nav-menu">
			<div class="widget-wrap">
				<nav role="navigation" id="related-navigation">
					<ul class="wp-nav-menu related-nav">
						%s
					</ul>
				</nav>
			</div>
		</div>', $children );
		}
		
		/**
		 * Output a slightly different related pages widget for 
		 * 		the PCP resources area
		 */
		function do_pcp_related_pages( $parent ) {
			if ( empty( $parent ) )
				return;
			
			$tmp = get_post( $parent );
			
			$args = array(
				'sort_column' => 'menu_order', 
				'title_li'    => '', 
				'echo'        => 0, 
				'post_type'   => $tmp->post_type, 
			);
			
?>
				<div class="related widget nav-menu pcp-menu">
					<div class="widget-wrap">
						<nav role="navigation" id="related-navigation">
							<ul class="wp-nav-menu related-nav">
<?php
			
			$children = get_pages( array( 'parent' => $parent, 'post_type' => 'page', 'sort_column' => 'menu_order' ) );
			foreach ( $children as $child ) {
				$classes = '';
				if ( $child->ID == get_the_ID() ) {
					$classes = ' current-menu-item current_page_item';
				} else {
					$tmp = get_pages( array( 'child_of' => $parent, 'post_type' => 'page' ) );
					foreach ( $tmp as $p ) {
						if ( $p->ID == get_the_ID() ) {
							$classes = ' current-menu-ancestor current_page_ancestor';
						}
					}
				}
				
				printf( '<li class="menu-item%1$s"><a href="%2$s" title="%3$s">%4$s</a>', $classes, get_permalink( $child->ID ), apply_filters( 'the_title_attribute', get_the_title( $child->ID ) ), apply_filters( 'the_title', get_the_title( $child->ID ) ) );
				
				$args['child_of'] = $child->ID;
				$menu = wp_list_pages( $args );
				if ( ! empty( $menu ) && ! is_wp_error( $menu ) ) {
					printf( '
								<ul class="sub-menu">
									%s
								</ul>', $menu );
				}
				
				echo '</li>';
			}
?>
							</ul>
						</nav>
					</div>
				</div>
<?php
		}
		
		/**
		 * Output the front page layout
		 */
		function do_front_page_widgets() {
			if ( ! is_front_page() )
				return;
			
			echo '<div class="front-content-area">';
			
			echo '<aside class="front-main"><div class="wrap">';
			dynamic_sidebar( 'front-main' );
			echo '</div></aside>';
			
			echo '<aside class="front-features"><div class="wrap">';
			dynamic_sidebar( 'front-features' );
			echo '</div></aside>';
			
			echo '<aside class="front-secondary"><div class="wrap">';
			dynamic_sidebar( 'front-secondary' );
			echo '</div></aside>';
			
			echo '</div>';
		}
	}
}

global $mc3;
$mc3 = MC3::instance();