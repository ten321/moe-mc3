# MC3 Theme

This is the WordPress theme used on the [Michigan Child Collaborative Care Program website](https://mc3.depressioncenter.org/).

## Version

1.0

## Contributors

* Designed by: [Moeller Design](http://moedesign.com)
* Developed and maintained by: [Ten-321 Enterprises](http://ten-321.com/)
